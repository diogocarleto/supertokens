package org.acme;

import io.quarkus.test.junit.QuarkusTest;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import quarkus.mongodb.panache.annotation.MongoUtils;
import quarkus.mongodb.panache.annotation.exception.OptimisticLockException;

import javax.inject.Inject;

@QuarkusTest
public class WalletRepositoryTests {

    @Inject
    MongoUtils mongoUtils;

    @Inject
    WalletRepository walletRepository;

    @BeforeEach
    public void beforeEach() {
        //Used to have entities clean before the tests
        mongoUtils.deleteAll(Wallet.class);
    }

    @Test
    public void update() {
        Wallet wallet = Wallet.of(Uuids.getNew(), 1);
        walletRepository.persistOrUpdate(wallet);
        assertEquals(0l, wallet.version);

        Wallet walletToUpdate = wallet.withAmount(5);
        walletRepository.persistOrUpdate(walletToUpdate);
        assertEquals(1, walletToUpdate.version);
    }

    @Test
    public void updateOutdated() {
        Wallet wallet = Wallet.of(Uuids.getNew(), 1);
        walletRepository.persistOrUpdate(wallet);

        assertThrows(OptimisticLockException.class, () -> walletRepository.persistOrUpdate(wallet.withVersion(-1l)));
    }
}
