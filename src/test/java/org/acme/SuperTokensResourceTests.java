package org.acme;

import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import quarkus.mongodb.panache.annotation.MongoUtils;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.IntStream;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@QuarkusTest
@Slf4j
public class SuperTokensResourceTests {

    private static final String BASE_PATH = "/supertokens";

    @Inject
    MongoUtils mongoUtils;

    @Inject
    WalletRepository walletRepository;

    @BeforeEach
    public void beforeEach() {
        //Used to have entities clean before the tests
        mongoUtils.deleteAll(Wallet.class);
    }

    private String buildPath(String path) {
        return BASE_PATH + path;
    }

    @Test
    public void getRandomInt() {
        Integer randomValue = getRandomValue();
        assertNotNull(randomValue);
    }

    @Test
    public void getRandomIntMultipleTimes() {
        Integer randomValue = getRandomValue();
        assertNotNull(randomValue);

        Integer randomValue2 = getRandomValue();
        assertNotEquals(randomValue, randomValue2);
    }

    @Test
    public void createWallet() {
        Wallet wallet = executePOSTRequest(buildPath("/"), Wallet.class);
        assertNotNull(wallet);
        assertNotNull(wallet.amount);
        assertNotNull(wallet.id);
        assertNotNull(wallet.version);
    }

    @Test
    public void updateWallet() {
        Wallet wallet = executePOSTRequest(buildPath("/"), Wallet.class);
        Wallet walletUpdated = executePUTRequest(buildPath("/"+wallet.id), Wallet.class);

        assertEquals(wallet.id, walletUpdated.id);
        assertNotEquals(wallet.amount, walletUpdated.amount);
        assertEquals(wallet.version+1, walletUpdated.version);
    }

    @Test
    public void updateWalletManyTimes() {
        Wallet wallet = executePOSTRequest(buildPath("/"), Wallet.class);
        IntStream.range(0, 10)
                .parallel()
                .forEach(i -> {
                    log.info("updating index: {}", i);
                    executePUTRequest(buildPath("/"+wallet.id), Wallet.class);
                });

        Wallet walletLastVersion = executeGETRequest(buildPath("/"+wallet.id), Wallet.class);
        assertEquals(10, walletLastVersion.version);
    }

    @DisplayName("should remove wallets with amount greater than 10")
    @Test
    public void checkWalletCleanerJob() throws Exception {

        List<Wallet> wallets = List.of(
                Wallet.of(Uuids.getNew(), 10),
                Wallet.of(Uuids.getNew(), 100),
                Wallet.of(Uuids.getNew(), 1));

        walletRepository.persist(wallets);
        Long totalWallets = executeGETRequest(buildPath("/count"), Long.class);

        assertEquals(3, totalWallets);
        Thread.sleep(5000);

        Long totalWalletsAfterJob = executeGETRequest(buildPath("/count"), Long.class);

        assertTrue(totalWallets > totalWalletsAfterJob);
    }

    private Integer getRandomValue() {
        return executeGETRequest(buildPath("/random"), Integer.class);
    }

    private <T> T executeGETRequest(String path, Class<T> returnType) {
        return given()
                .when().get(path)
                .then()
                .statusCode(200)
                .extract().body()
                .as(returnType);
    }

    private <T> T executePOSTRequest(String path, Class<T> returnType) {
        return given()
                .when().post(path)
                .then()
                .statusCode(200)
                .extract().body()
                .as(returnType);
    }

    private <T> T executePUTRequest(String path, Class<T> returnType) {
        return given()
                .when().put(path)
                .then()
                .statusCode(200)
                .extract().body()
                .as(returnType);
    }
}
