package io.quarkus.mongodb.panache.runtime;

import com.mongodb.MongoWriteException;
import io.quarkus.mongodb.panache.common.MongoEntity;
import io.quarkus.test.junit.QuarkusTest;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.acme.Uuids;
import org.bson.codecs.pojo.annotations.BsonId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import quarkus.mongodb.panache.annotation.MongoUtils;
import quarkus.mongodb.panache.annotation.Version;
import quarkus.mongodb.panache.annotation.exception.OptimisticLockException;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@QuarkusTest
public class MongoUtilsOptimisticLockControlIntegrationTests {

    @Inject
    MongoUtils mongoUtils;

    @BeforeEach
    public void beforeEach() {
        mongoUtils.deleteAll(Car.class, CarV.class);
    }

    @Test
    public void persistDocumentWithoutVersion() {
        Car carWithoutVersion = buildDefault();
        mongoUtils.persist(carWithoutVersion);

        Car carDb = mongoUtils.findById(Car.class, carWithoutVersion.id);
        assertNotNull(carDb);
        assertEquals(carWithoutVersion, carDb);
    }

    @Test
    public void persistDocumentWithVersion() {
        CarV carWithVersion = buildDefaultV();
        mongoUtils.persist(carWithVersion);

        CarV carDb = mongoUtils.findById(CarV.class, carWithVersion.id);
        assertNotNull(carDb);
        assertEquals(0, carDb.version);
    }

    @Test
    public void updateDocumentWithoutVersion() {
        Car carWithoutVersion = buildDefault();
        mongoUtils.persist(carWithoutVersion);

        Car carDb = mongoUtils.findById(Car.class, carWithoutVersion.id);
        assertNotNull(carDb);
        assertEquals(carWithoutVersion, carDb);

        carWithoutVersion = carWithoutVersion.toBuilder()
                .modelYear(2022).build();
        mongoUtils.update(carWithoutVersion);

        carDb = mongoUtils.findById(Car.class, carWithoutVersion.id);
        assertNotNull(carDb);
        assertEquals(carWithoutVersion, carDb);
    }

    @Test
    public void updateDocumentWithVersion() {
        CarV car = buildDefaultV();
        mongoUtils.persist(car);

        CarV carDb = mongoUtils.findById(CarV.class, car.id);
        assertEquals(0, carDb.version);

        car = carDb.toBuilder()
                .modelYear(2022).build();
        mongoUtils.update(car);

        carDb = mongoUtils.findById(CarV.class, car.id);
        assertEquals(1, carDb.version);
    }

    @Test
    public void updateDocumentWithOutdatedVersion() {
        CarV car = buildDefaultV();
        mongoUtils.persist(car);

        CarV carDb = mongoUtils.findById(CarV.class, car.id);
        assertNotNull(carDb);
        assertEquals(car, carDb);

        OptimisticLockException optimisticLockException = assertThrows(OptimisticLockException.class, () -> {
            CarV car2 = CarV.builder()
                    .id(carDb.id)
                    .modelYear(2021)
                    .version(-1l).build();
            mongoUtils.update(car2);
        });

        assertNotNull(optimisticLockException.getMessage());
    }

    @Test
    public void updateNonExistingDocumentWithVersion() {
        CarV car = buildDefaultV();
        mongoUtils.persist(car);

        CarV carDb = mongoUtils.findById(CarV.class, car.id);
        assertEquals(0, carDb.version);

        assertThrows(OptimisticLockException.class, () -> {
            CarV carV = CarV.builder()
                    .id(Uuids.getNew())
                    .modelYear(2022).build();
            mongoUtils.update(carV);
        });

        carDb = mongoUtils.findById(CarV.class, car.id);
        assertEquals(0, carDb.version);
    }

    @Test
    public void persistOrUpdateDocumentWithoutVersion() {
        Car carWithoutVersion = buildDefault();
        mongoUtils.persistOrUpdate(carWithoutVersion);

        Car carDb = mongoUtils.findById(Car.class, carWithoutVersion.id);
        assertNotNull(carDb);
        assertEquals(carWithoutVersion, carDb);

        carWithoutVersion = carWithoutVersion.toBuilder()
                .modelYear(2022).build();
        mongoUtils.persistOrUpdate(carWithoutVersion);

        carDb = mongoUtils.findById(Car.class, carWithoutVersion.id);
        assertNotNull(carDb);
        assertEquals(carWithoutVersion, carDb);
    }

    @Test
    public void persistOrUpdateDocumentWithVersion() {
        //add the car
        CarV car = buildDefaultV();
        mongoUtils.persistOrUpdate(car);
        CarV carDb = mongoUtils.findById(CarV.class, car.id);
        assertEquals(0, carDb.version);

        //must update the car
        car = carDb.toBuilder()
                .modelYear(2022).build();
        mongoUtils.persistOrUpdate(car);

        carDb = mongoUtils.findById(CarV.class, car.id);
        assertEquals(1, carDb.version);
        assertEquals(2022, carDb.modelYear);
    }

    @Test
    public void persistOrUpdateDocumentWithOutdatedVersion() {
        //persist the car
        CarV car = buildDefaultV();
        mongoUtils.persistOrUpdate(car);
        CarV carDb = mongoUtils.findById(CarV.class, car.id);
        assertNotNull(carDb);
        assertEquals(car, carDb);

        //fail trying to update without version
        MongoWriteException mongoWriteException = assertThrows(MongoWriteException.class, () -> {
            CarV car2 = CarV.builder()
                    .id(carDb.id)
                    .modelYear(2021).build();
            mongoUtils.persistOrUpdate(car2);
        });

        assertTrue(mongoWriteException.getMessage().contains("E11000 duplicate key error collection"));
    }

    @Test
    public void persistOrUpdateConcurrentReqs() {
        Map<Integer, Boolean> mapStatus = new HashMap<>();
        String id = Uuids.getNew();
        CarV carV = new CarV(id, 2021, null);
        mongoUtils.persist(carV);

        List<CarV> carVListToUpdate = IntStream.range(0, 10)
                .mapToObj(i -> CarV.builder()
                        .id(id)
                        .modelYear(i)
                        .version(0l).build()).collect(Collectors.toList());

        carVListToUpdate.parallelStream()
                .forEach(carV1 -> {
                    try {
                        mongoUtils.persistOrUpdate(carV1);
                        mapStatus.put(carV1.modelYear, true);
                    } catch (OptimisticLockException ex) {
                        mapStatus.put(carV1.modelYear, false);
                    }
                });

        List<CarV> carVList2 = mongoUtils.findAll(CarV.class).list();
        assertEquals(1, carVList2.size());

        List<Integer> updateWithSuccessList = mapStatus.entrySet().stream()
                .filter(entry -> entry.getValue())
                .map(entry -> entry.getKey())
                .collect(Collectors.toList());

        assertEquals(1, updateWithSuccessList.size());
        assertEquals(updateWithSuccessList.get(0), carVList2.get(0).modelYear);
    }

    @Test
    public void updateConcurrentReqs() {
        Map<Integer, Boolean> mapStatus = new HashMap<>();
        String id = Uuids.getNew();
        CarV carV = new CarV(id, 2021, null);
        mongoUtils.persist(carV);

        List<CarV> carVListToUpdate = IntStream.range(0, 10)
                .mapToObj(i -> CarV.builder()
                        .id(id)
                        .modelYear(i)
                        .version(0l).build()).collect(Collectors.toList());

        carVListToUpdate.parallelStream()
                .forEach(carV1 -> {
                    try {
                        mongoUtils.update(carV1);
                        mapStatus.put(carV1.modelYear, true);
                    } catch (OptimisticLockException ex) {
                        mapStatus.put(carV1.modelYear, false);
                    }
                });

        List<CarV> carVList2 = mongoUtils.findAll(CarV.class).list();
        assertEquals(1, carVList2.size());

        List<Integer> updateWithSuccessList = mapStatus.entrySet().stream()
                .filter(entry -> entry.getValue())
                .map(entry -> entry.getKey())
                .collect(Collectors.toList());

        assertEquals(1, updateWithSuccessList.size());
        assertEquals(updateWithSuccessList.get(0), carVList2.get(0).modelYear);
    }

    @Test
    public void bulkPersistWithoutVersion() {
        List<Car> carList = IntStream.range(0, 10)
                .mapToObj(i -> new Car(Uuids.getNew(), i))
                .collect(Collectors.toList());
        mongoUtils.persist(carList);

        List<Car> carDbList = mongoUtils.findAll(Car.class).list();
        assertEquals(10, carDbList.size());
    }

    @Test
    public void bulkPersistWithVersion() {
        List<CarV> carList = IntStream.range(0, 10)
                .mapToObj(i -> new CarV(Uuids.getNew(), i, null))
                .collect(Collectors.toList());
        mongoUtils.persist(carList);

        List<CarV> carDbList = mongoUtils.findAll(CarV.class).list();
        assertEquals(10, carDbList.size());
        carDbList.forEach(carV -> assertEquals(0l, carV.version));
    }

    @Test
    public void bulkUpdateWithoutVersion() {
        String id = Uuids.getNew();
        mongoUtils.persist(new Car(id, 2021));

        List<Car> carList = IntStream.range(0, 10)
                .mapToObj(i -> new Car(id, i))
                .collect(Collectors.toList());
        mongoUtils.update(carList);

        List<Car> carDbList = mongoUtils.findAll(Car.class).list();
        assertEquals(1, carDbList.size());
    }

    @Test
    public void bulkUpdateWithVersion() {
        //add one
        String id = Uuids.getNew();
        mongoUtils.persist(new CarV(id, 2021, null));

        //update this one and check the values
        mongoUtils.update(Arrays.asList(new CarV(id, 0, 0l)));

        List<CarV> carDbList = mongoUtils.findAll(CarV.class).list();
        assertEquals(1, carDbList.size());
        CarV carV = carDbList.get(0);
        assertEquals(0, carV.modelYear);
        assertEquals(1l, carV.version);

        //update again
        mongoUtils.update(Arrays.asList(new CarV(id, 2, 1l)));
        carDbList = mongoUtils.findAll(CarV.class).list();
        assertEquals(1, carDbList.size());
        carV = carDbList.get(0);
        assertEquals(2, carV.modelYear);
        assertEquals(2l, carV.version);

        //try to update with wrong version
        assertThrows(OptimisticLockException.class,
                () -> mongoUtils.update(Arrays.asList(new CarV(id, 1, 1l))));

        //update using a list containing more elements, must fail
        List<CarV> carList = IntStream.range(0, 10)
                .mapToObj(i -> new CarV(id, i, 2l))
                .collect(Collectors.toList());
        assertThrows(OptimisticLockException.class, () -> mongoUtils.update(carList));
    }

    @Test
    public void bulkPersistOrUpdateWithoutVersion() {
        List<Car> carList = IntStream.range(0, 10)
                .mapToObj(i -> new Car(Uuids.getNew(), i))
                .collect(Collectors.toList());
        mongoUtils.persistOrUpdate(carList);

        List<Car> carDbList = mongoUtils.findAll(Car.class).list();
        assertEquals(10, carDbList.size());
    }

    @Test
    public void bulkPersistOrUpdateWithVersion() {
        String id = Uuids.getNew();
        mongoUtils.persist(new CarV(id, 2021, null));

        List<CarV> carList = IntStream.range(0, 1)
                .mapToObj(i -> new CarV(id, i, 0l))
                .collect(Collectors.toList());
        mongoUtils.persistOrUpdate(carList);

        List<CarV> carDbList = mongoUtils.findAll(CarV.class).list();
        assertEquals(1, carDbList.size());
        assertEquals(0, carDbList.get(0).modelYear);
        assertEquals(1l, carDbList.get(0).version);
    }

    @Test
    public void deleteWithoutVersionNonExistentDocument() {
        Car car = buildDefault();
        mongoUtils.delete(car);
    }

    @Test
    public void deleteWithVersionNonExistentDocument() {
        CarV car = buildDefaultV();
        assertThrows(OptimisticLockException.class, () -> mongoUtils.delete(car));
    }

    private CarV buildDefaultV() {
        return CarV.builder()
                .id(Uuids.getNew())
                .modelYear(2021).build();
    }

    private Car buildDefault() {
        return Car.builder()
                .id(Uuids.getNew())
                .modelYear(2021).build();
    }

    @ToString
    @EqualsAndHashCode
    @NoArgsConstructor(force = true)
    public abstract static class Vehicle<E> {

        @BsonId
        public String id;
        public Integer modelYear;

        public Vehicle(String id, Integer modelYear) {
            this.id = id;
            this.modelYear = modelYear;
        }
    }

    @EqualsAndHashCode(callSuper = true)
    @NoArgsConstructor(force = true)
    @MongoEntity
    public static class Car extends Vehicle<Car> {

        @Builder(toBuilder = true)
        public Car(String id, Integer modelYear) {
            super(id, modelYear);
        }
    }

    @ToString(callSuper = true)
    @EqualsAndHashCode
    @NoArgsConstructor(force = true)
    @MongoEntity
    public static class CarV extends Vehicle<CarV> {

        @Version
        public Long version;

        @Builder(toBuilder = true)
        public CarV(String id, Integer modelYear, Long version) {
            super(id, modelYear);
            this.version = version;
        }
    }
}
