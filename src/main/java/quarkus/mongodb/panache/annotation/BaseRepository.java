package quarkus.mongodb.panache.annotation;

import io.quarkus.arc.Arc;
import io.quarkus.mongodb.panache.PanacheMongoRepositoryBase;
import io.quarkus.mongodb.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import org.bson.Document;

import javax.annotation.PostConstruct;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.stream.Stream;

/**
 * Not required after when running inside the Quarkus core.
 */
public class BaseRepository<Entity, Id> implements PanacheMongoRepositoryBase<Entity, Id> {

    private Class<Entity> entityClass;

    @PostConstruct
    public void postConstruct() {
        entityClass = getEntityClazz();
    }

    @Override
    public long deleteAll() {
        getMongoUtils().deleteAll(entityClass);
        return 0l;
    }
    
    @Override
    public PanacheQuery<Entity> findAll() {
        return findAll(entityClass);
    }

    @Override
    public PanacheQuery<Entity> findAll(Sort sort) {
        return findAll(sort);
    }

    public <Entity> long deleteAll(Class<Entity> entityClass) {
        getMongoUtils().deleteAll(entityClass);
        return 0l;
    }

    public PanacheQuery<Entity> findAll(Class<Entity> entityClass) {
        return getMongoUtils().findAll(entityClass);
    }

    @Override
    public Entity findById(Id id) {
        return getMongoUtils().findById(entityClass, (String) id);
    }

    @Override
    public void persist(Entity entity) {
        getMongoUtils().persist(entity);
    }

    @Override
    public void persist(Iterable<Entity> entities) {
        getMongoUtils().persist(entities);
    }

    @Override
    public void persist(Stream<Entity> entities) {
        getMongoUtils().persist(entities);
    }

    @Override
    public void persist(Entity firstEntity, Entity... entities) {
        getMongoUtils().persist(firstEntity, entities);
    }

    @Override
    public void persistOrUpdate(Entity entity) {
        getMongoUtils().persistOrUpdate(entity);
    }

    @Override
    public void persistOrUpdate(Iterable<Entity> entities) {
        getMongoUtils().persistOrUpdate(entities);
    }

    @Override
    public void persistOrUpdate(Stream<Entity> entities) {
        getMongoUtils().persistOrUpdate(entities);
    }

    @Override
    public void persistOrUpdate(Entity firstEntity, Entity... entities) {
        getMongoUtils().persistOrUpdate(firstEntity, entities);
    }

    @Override
    public void update(Entity entity) {
        getMongoUtils().update(entity);
    }

    @Override
    public void update(Iterable<Entity> entities) {
        getMongoUtils().update(entities);
    }

    @Override
    public void update(Stream<Entity> entities) {
        getMongoUtils().update(entities);
    }

    @Override
    public void update(Entity firstEntity, Entity... entities) {
        getMongoUtils().update(firstEntity, entities);
    }

    @Override
    public void delete(Entity entity) {
        getMongoUtils().delete(entity);
    }

    private Class<Entity> getEntityClazz() {
        //this if is required when repository uses cache.
        Type genericSuperClass = null;
        if (ParameterizedType.class.isAssignableFrom(getClass().getGenericSuperclass().getClass())) {
            genericSuperClass = getClass().getGenericSuperclass();
        } else {
            genericSuperClass = ((Class) getClass().getGenericSuperclass()).getGenericSuperclass();
        }

        return (Class<Entity>) ((ParameterizedType) genericSuperClass)
                .getActualTypeArguments()[0];
    }

    protected MongoUtils getMongoUtils() {
        return Arc.container().instance(MongoUtils.class).get();
    }

    //the following is because there is no implementation in Panache that allow us to sort by $meta, such as
    // { score: { $meta: "textScore" } }
    //so we need to call find(Document, Document)
    /**
     * We should have a query like <code>{'firstname': ?1, 'lastname': ?2}</code> for native one
     * and like <code>firstname = ?1</code> for PanacheQL one.
     */
    protected Document buildQuery(Class<?> entityClass, String query, Object... params) {
        return getMongoUtils().bindFilter(entityClass, query, params);
    }
}
