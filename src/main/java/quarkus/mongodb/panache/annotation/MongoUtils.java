package quarkus.mongodb.panache.annotation;


import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.InsertOneModel;
import com.mongodb.client.model.ReplaceOneModel;
import com.mongodb.client.model.ReplaceOptions;
import com.mongodb.client.model.WriteModel;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import io.quarkus.arc.Unremovable;
import io.quarkus.mongodb.panache.PanacheQuery;
import io.quarkus.mongodb.panache.common.runtime.MongoOperations;
import io.quarkus.mongodb.panache.runtime.JavaMongoOperations;
import io.quarkus.panache.common.Parameters;
import io.quarkus.panache.common.Sort;
import org.bson.BsonDocument;
import org.bson.BsonDocumentWriter;
import org.bson.BsonNull;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.codecs.Codec;
import org.bson.codecs.EncoderContext;
import quarkus.mongodb.panache.annotation.exception.OptimisticLockException;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
@Unremovable
public class MongoUtils {

    private static final String ID = "_id";
    private static final String VERSION = "version";

    private MongoOperations operations = JavaMongoOperations.INSTANCE;

    public MongoCollection mongoCollection(Class<?> entityClass) {
        return operations.mongoCollection(entityClass);
    }

    public void deleteAll(Class<?>... classes) {
        for (Class<?> clazz : classes) {
            operations.deleteAll(clazz);
        }
    }

    public <E> E findById(Class<E> entityClass, String id) {
        return (E) operations.findById(entityClass, id);
    }

    public <E> PanacheQuery<E> findAll(Class<E> entityClass) {
        return (PanacheQuery<E>) operations.findAll(entityClass);
    }

    public <E> PanacheQuery<E> findAll(Class<E> entityClass, Sort sort) {
        return (PanacheQuery<E>) operations.findAll(entityClass, sort);
    }

    public <E> PanacheQuery<E> find(Class<E> entityClass, String query, Parameters params) {
        return (PanacheQuery<E>) operations.find(entityClass, query, params);
    }

    public void persist(Object entity) {
        MongoCollection collection = operations.mongoCollection(entity.getClass());

        VersionHandler versionUtil = VersionHandler.of(entity);
        if (versionUtil.containsVersionAnnotation()) {
            versionUtil.adjustVersionValue();
        }

        collection.insertOne(entity);
    }

    public void persist(Iterable<?> entities) {
        // not all iterables are re-traversal, so we traverse it once for copying inside a list
        List<Object> objects = new ArrayList<>();
        for (Object entity: entities) {
            VersionHandler versionUtil = VersionHandler.of(entity);
            if (versionUtil.containsVersionAnnotation()) {
                versionUtil.adjustVersionValue();
            }
            objects.add(entity);
        }

        if (objects.size() > 0) {
            // get the first entity to be able to retrieve the collection with it
            Object firstEntity = objects.get(0);
            MongoCollection collection = mongoCollection(firstEntity.getClass());
            persist(collection, objects);
        }
    }

    public void persist(Stream<?> entities) {
        List<Object> objects = entities.collect(Collectors.toList());
        if (objects.size() > 0) {
            // get the first entity to be able to retrieve the collection with it
            Object firstEntity = objects.get(0);
            MongoCollection collection = mongoCollection(firstEntity.getClass());
            persist(collection, objects);
        }
    }

    public void persist(Object firstEntity, Object... entities) {
        MongoCollection collection = mongoCollection(firstEntity.getClass());
        if (entities == null || entities.length == 0) {
            persist(collection, firstEntity);
        } else {
            List<Object> entityList = new ArrayList<>();
            entityList.add(firstEntity);
            entityList.addAll(Arrays.asList(entities));
            persist(collection, entityList);
        }
    }

    public void update(Object entity) {
        MongoCollection collection = operations.mongoCollection(entity.getClass());
        update(collection, entity);
    }

    public void update(Iterable<?> entities) {
        // not all iterables are re-traversal, so we traverse it once for copying inside a list
        List<Object> objects = new ArrayList<>();
        for (Object entity: entities) {
            objects.add(entity);
        }

        if (objects.size() > 0) {
            // get the first entity to be able to retrieve the collection with it
            Object firstEntity = objects.get(0);
            MongoCollection collection = mongoCollection(firstEntity.getClass());
            update(collection, objects);
        }
    }

    public void update(Object firstEntity, Object... entities) {
        MongoCollection collection = mongoCollection(firstEntity.getClass());
        if (entities == null || entities.length == 0) {
            update(collection, firstEntity);
        } else {
            List<Object> entityList = new ArrayList<>();
            entityList.add(firstEntity);
            entityList.addAll(Arrays.asList(entities));
            update(collection, entityList);
        }
    }

    public void update(Stream<?> entities) {
        List<Object> objects = entities.collect(Collectors.toList());
        if (objects.size() > 0) {
            // get the first entity to be able to retrieve the collection with it
            Object firstEntity = objects.get(0);
            MongoCollection collection = mongoCollection(firstEntity.getClass());
            update(collection, objects);
        }
    }

    public void persistOrUpdate(Object entity) {
        MongoCollection collection = operations.mongoCollection(entity.getClass());
        //we transform the entity as a document first
        BsonDocument document = getBsonDocument(collection, entity);

        //then we get its id field and create a new Document with only this one that will be our replace query
        BsonValue id = document.get(ID);
        if (id == null) {
            //insert with autogenerated ID
            persist(entity);
        } else {
            VersionHandler versionUtil = VersionHandler.of(entity);
            if(!versionUtil.containsVersionAnnotation()) {
                BsonDocument query = buildUpdateQueryWithoutVersion(entity);
                collection.replaceOne(query, entity, new ReplaceOptions().upsert(true));
            } else if (versionUtil.containsVersionAnnotationAndValue()) {
                update(entity);
            } else if(!versionUtil.containsVersionValue()) {
                BsonDocument query = buildUpdateQuery(entity);
                versionUtil.adjustVersionValue();
                collection.replaceOne(query, entity, new ReplaceOptions().upsert(true));
            }
        }
    }

    public void persistOrUpdate(Iterable<?> entities) {
        // not all iterables are re-traversal, so we traverse it once for copying inside a list
        List<Object> objects = new ArrayList<>();
        for (Object entity : entities) {
            objects.add(entity);
        }

        if (objects.size() > 0) {
            // get the first entity to be able to retrieve the collection with it
            Object firstEntity = objects.get(0);
            MongoCollection collection = mongoCollection(firstEntity.getClass());
            persistOrUpdate(collection, objects);
        }
    }

    public void persistOrUpdate(Object firstEntity, Object... entities) {
        MongoCollection collection = mongoCollection(firstEntity.getClass());
        if (entities == null || entities.length == 0) {
            persistOrUpdate(collection, firstEntity);
        } else {
            List<Object> entityList = new ArrayList<>();
            entityList.add(firstEntity);
            entityList.addAll(Arrays.asList(entities));
            persistOrUpdate(collection, entityList);
        }
    }

    public void persistOrUpdate(Stream<?> entities) {
        List<Object> objects = entities.collect(Collectors.toList());
        if (objects.size() > 0) {
            // get the first entity to be able to retrieve the collection with it
            Object firstEntity = objects.get(0);
            MongoCollection collection = mongoCollection(firstEntity.getClass());
            persistOrUpdate(collection, objects);
        }
    }

    public void delete(Object entity) {
        VersionHandler versionHandler = VersionHandler.of(entity);
        MongoCollection collection = mongoCollection(entity.getClass());
        BsonDocument query = buildUpdateQuery(entity);
        DeleteResult deleteResult = collection.deleteOne(query);
        if (versionHandler.containsVersionAnnotation() && deleteResult.getDeletedCount() == 0) {
            throwOptimisticLockException(entity);
        }
    }

    public Document bindFilter(Class<?> entityClass, String query, Object... params) {
        return Document.parse(JavaMongoOperations.INSTANCE.bindFilter(entityClass, query, params));
    }

    //
    //private stuff

    private void persist(MongoCollection collection, List<Object> entities) {
        collection.insertMany(entities);
    }

    private void update(MongoCollection collection, Object entity) {
        VersionHandler versionHandler = VersionHandler.of(entity);
        //we didn't add to the list entities that is under version control and has no version value.
//        if (versionHandler.containsVersionAnnotation() && !versionHandler.containsVersionValue()) {
//            return;
//        }

        BsonDocument query = buildUpdateQuery(entity);
        versionHandler.adjustVersionValue();
        UpdateResult updateResult = collection.replaceOne(query, entity);
        if (versionHandler.containsVersionAnnotation() && updateResult.getModifiedCount() == 0) {
            throwOptimisticLockException(entity);
        }
    }

    private void update(MongoCollection collection, List<Object> entities) {
        for (Object entity : entities) {
            update(collection, entity);
        }
    }

    private void persistOrUpdate(MongoCollection collection, List<Object> entities) {
        //this will be an ordered bulk: it's less performant than a unordered one but will fail at the first failed write
        List<WriteModel> bulk = new ArrayList<>();
        for (Object entity : entities) {
            //we transform the entity as a document first
            BsonDocument document = getBsonDocument(collection, entity);

            //then we get its id field and create a new Document with only this one that will be our replace query
            BsonValue id = document.get(ID);
            if (id == null) {
                //here we handle the Version
                VersionHandler versionUtil = VersionHandler.of(entity);
                if (versionUtil.containsVersionAnnotation()) {
                    versionUtil.adjustVersionValue();
                }
                //insert with autogenerated ID
                bulk.add(new InsertOneModel(entity));
            } else {
                VersionHandler versionHandler = VersionHandler.of(entity);
                if(!versionHandler.containsVersionAnnotation()) {
                    BsonDocument query = buildUpdateQueryWithoutVersion(entity);
                    bulk.add(new ReplaceOneModel(query, entity, new ReplaceOptions().upsert(true)));
                } else {
                    if (versionHandler.containsVersionAnnotationAndValue()) {
                        //it will ignore in silence not matching updates.
                        BsonDocument query = buildUpdateQuery(entity);
                        versionHandler.adjustVersionValue();
                        bulk.add(new ReplaceOneModel(query, entity));
                    } else if(!versionHandler.containsVersionValue()) {
                        //only try upsert when there is no version value
                        BsonDocument query = buildUpdateQuery(entity);
                        versionHandler.adjustVersionValue();
                        bulk.add(new ReplaceOneModel(query, entity, new ReplaceOptions().upsert(true)));
                    }
                }
            }
        }

        collection.bulkWrite(bulk);
    }

    /**
     * We check if is needed to throw a OptimisticLockException.
     */
    private void throwOptimisticLockException(Object entity) {
        StringBuilder errorMsg = new StringBuilder("Was not possible to update entity: ")
                .append(entity.toString());
        throw new OptimisticLockException(errorMsg.toString());
    }

    /**
     * We build the query that will be used to update the document, can have id, or id/version.
     */
    private BsonDocument buildUpdateQuery(Object entity) {
        MongoCollection collection = operations.mongoCollection(entity.getClass());
        //we transform the entity as a document first
        BsonDocument document = getBsonDocument(collection, entity);

        //then we get its id field and create a new Document with only this one that will be our replace query
        BsonValue id = document.get(ID);

        VersionHandler versionUtil = VersionHandler.of(entity);
        if (versionUtil.containsVersionAnnotation()) {
            BsonValue version = document.get(VERSION);
            if (version == null) {
                version = new BsonNull();
            }
            return new BsonDocument().append(ID, id).append(VERSION, version);
        }
        return new BsonDocument().append(ID, id);
    }

    private BsonDocument buildUpdateQueryWithoutVersion(Object entity) {
        MongoCollection collection = operations.mongoCollection(entity.getClass());
        //we transform the entity as a document first
        BsonDocument document = getBsonDocument(collection, entity);

        //then we get its id field and create a new Document with only this one that will be our replace query
        BsonValue id = document.get(ID);
        return new BsonDocument().append(ID, id);
    }

    private BsonDocument getBsonDocument(MongoCollection collection, Object entity) {
        BsonDocument document = new BsonDocument();
        Codec codec = collection.getCodecRegistry().get(entity.getClass());
        codec.encode(new BsonDocumentWriter(document), entity, EncoderContext.builder().build());
        return document;
    }
}
