package org.acme;

import lombok.extern.slf4j.Slf4j;
import quarkus.mongodb.panache.annotation.exception.OptimisticLockException;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;
import java.util.Random;

@Path("/supertokens")
@Produces(MediaType.APPLICATION_JSON)
@Slf4j
public class SuperTokensResource {

    @Inject
    WalletRepository walletRepository;

    private static Random random = new Random(1000l);

    @GET
    @Path("/random")
    public Integer getRandomNumber() {
        return random.nextInt();
    }

    @PUT
    @Path("/{id}")
    public Response update(@PathParam("id") String id) {
        Optional<Wallet> walletOptional = _update(id);

        if (walletOptional.isEmpty()) {
            return Response.noContent().build();
        }

        return Response.ok(walletOptional.get()).build();
    }

    private Optional<Wallet> _update(String id) {
        Optional<Wallet> walletOptional = walletRepository.findByIdOptional(id);

        if (walletOptional.isEmpty()) {
            return walletOptional;
        }

        Wallet wallet = walletOptional.get();
        wallet.amount += getRandomNumber();

        try {
            walletRepository.update(wallet);
            return Optional.of(wallet);
        } catch (OptimisticLockException ex) {
            log.warn("WILL TRY TO UPDATE AGAIN: {}", wallet.version);
            return _update(id);
        }
    }

    @POST
    @Path("/")
    public Wallet create() {
        Wallet wallet = Wallet.of(Uuids.getNew(), 1);
        walletRepository.persist(wallet);
        return wallet;
    }

    @GET
    @Path("/{id}")
    public Response findById(@PathParam("id") String id) {
        Optional<Wallet> walletOptional = walletRepository.findByIdOptional(id);

        if (walletOptional.isEmpty()) {
            return Response.noContent().build();
        }

        return Response.ok(walletOptional.get()).build();
    }

    @GET
    @Path("/count")
    public Long countWallets() {
        return walletRepository.findAll().count();
    }
}
