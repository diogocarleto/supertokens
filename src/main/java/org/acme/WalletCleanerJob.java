package org.acme;

import io.quarkus.scheduler.Scheduled;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.TimeUnit;

@ApplicationScoped
@Slf4j
public class WalletCleanerJob {

    @Inject
    WalletRepository walletRepository;

    @Scheduled(every="2s", delay = 1l, delayUnit = TimeUnit.SECONDS)
    public void cleanWallet() {
        log.info("starting job to clean Wallets with amount greater that 10");
        List<Wallet> list = walletRepository.findAmountGreaterThan(10);
        list.forEach(wallet -> walletRepository.delete(wallet));
    }
}
