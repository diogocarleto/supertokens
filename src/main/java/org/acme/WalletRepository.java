package org.acme;

import quarkus.mongodb.panache.annotation.BaseRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class WalletRepository extends BaseRepository<Wallet, String> {

    public List<Wallet> findAmountGreaterThan(Integer amount) {
        return find("{'amount': {$gt: ?1}}", amount).list();
    }
}
