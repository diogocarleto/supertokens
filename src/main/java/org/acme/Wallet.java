package org.acme;

import io.quarkus.mongodb.panache.common.MongoEntity;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.With;
import org.bson.codecs.pojo.annotations.BsonId;
import quarkus.mongodb.panache.annotation.Version;

@MongoEntity
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor(force = true)
@With
@EqualsAndHashCode //used because Quarkus generates get/setter already
public class Wallet {

    @BsonId
    public String id;
    public Integer amount;
    @Version
    public Long version;

    public static Wallet of(String id, Integer amount) {
        return Wallet.of(id, amount, null);
    }
}
