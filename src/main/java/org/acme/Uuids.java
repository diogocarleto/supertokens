package org.acme;

import java.util.UUID;

public class Uuids {

    public static String getNew() {
        return UUID.randomUUID().toString();
    }
}
